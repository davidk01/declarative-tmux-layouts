require "decmux/version"

module Decmux

##
# Superclass of all windows/panes.

class Node

  def initialize; @commands, @children = [], []; end

  ##
  # The first child to call +split+ signals its parent so that we know which
  # child to put first when recursively generating commands. We need this because of how pane numbering works.

  def first_split=(name); @first_split ||= name; end

  ##
  # Commands that will be used with 'send-keys'.

  def cmd(command); @commands << command; end

  ##
  # Sequence of 'split-window' commands with the correct pane targets.

  def tmux_split_commands
    init_cmd, target = "", "-t#{@counter}"
    # transposed the children if the right/bottom pane was the first to call +split+
    @children.reverse! if [:right, :bottom].member?(@first_split)
    case (c = @children.first) && c.name
    when :left, :right
      init_cmd = "tmux split-window -h #{target}\n"
    when :top, :bottom
      init_cmd = "tmux split-window -v #{target}\n"
    end
    init_cmd + @children.flat_map(&:tmux_split_commands).join("\n")
  end

  ##
  # Theoretically we can do everything in one pass as we are traversing the tree to generate
  # 'split-window' commands but it is more clear to do things in two passes.

  def send_key_commands(accumulated_commands = [])
    acc_commands, target = accumulated_commands + @commands, "-t#{@counter}"
    @children.empty? ?
     acc_commands.map {|c| "tmux send-keys #{target} '#{c}; '"}.join("\n") + "\ntmux send-keys #{target} Enter" :
     @children.flat_map {|c| c.send_key_commands(acc_commands)}.join("\n")
  end

  ##
  # We need to keep track of the highest pane number because we need it when splitting,
  # e.g. n -> ([left, top] n, [right, bottom] n + 1)

  def incr_pane_counter; (p = @parent) ? p.incr_pane_counter : @pane_counter += 1; end

  ##
  # Called in +split+ to create :top/:bottom, :left/:right readers.

  def create_readers(*symbols); singleton_class.class_exec { attr_reader *symbols }; end

  ##
  # We enforce various invariants with the help of the singleton class. 1) +split+ can only
  # be called at most once, 2) child node readers are only defined for either left/right or
  # top/bottom but not both, 3) subsequent command invocations are sent to the children by
  # redefining +cmd+ in the parent node.

  def split(direction, &blk)
    singleton_class.class_exec do
      def split(direction, &blk)
        raise StandardError, "+split+ can only be called once."
      end
    end
    case direction
    when :horizontal
      @children = [@left = Pane.new(@counter, :left, self), 
       @right = Pane.new(incr_pane_counter, :right, self)]
      create_readers :left, :right
    when :vertical
      @children = [@top = Pane.new(@counter, :top, self),
       @bottom = Pane.new(incr_pane_counter, :bottom, self)]
      create_readers :top, :bottom
    else
      raise StandardError, "Unknown direction: :#{direction}."
    end
    instance_exec(&blk) if blk
    singleton_class.instance_exec(@children) do |children|
      define_method(:cmd) do |command|
        children.each {|c| c.cmd(command)}
      end
    end
  end

end

##
# Top level class that represents a window. Keeps track of the required tree structure and some extra bits
# of information for splitting windows/panes and running commands inside those panes, e.g. name, highest pane number.

class RootNode < Node

  def initialize(name)
    super(); @window_name, @pane_counter, @counter = name, 0, 0
  end

end

##
# When we split in either direction we create an instance of a +Pane+. We keep track
# of the pane number for command targeting along with a reference to the parent for sending
# some information up the chain.

class Pane < Node

  ##
  # We need the name (:left/:right/:top/:bottom) because it is used during 'split-window' command 
  # generation. We determine the orientation by querying the name:
  # :left/:right -> 'split-window -h', :top/:bottom -> 'split-window -v'

  attr_reader :name

  def initialize(counter, name, parent)
    super(); @counter, @name, @parent = counter, name, parent
  end

  ##
  # Make sure the parent knows which child called +split+ first so that we generate 'split-window'
  # commands in the correct target order.

  def split(direction, &blk)
    @parent.first_split = @name; super
  end

end

##
# Entry point for the configuration. More specifically the entry point is +Decmux::Layout::configure!+.

class Layout

  def initialize; @windows = {}; end

  ##
  # Creates a root node, i.e. a window in tmux parlance.

  def window(name, &blk)
    raise StandardError, "A window by that name already exists: #{name}." if @windows[name]
    @windows[name] = (new_window = RootNode.new(name))
    new_window.instance_exec(&blk)
  end

  ##
  # Entry point for configuring the layout and starting a new session.

  def self.configure!(&blk)
    (inst = new).instance_exec(&blk); inst.run!
  end

  ##
  # Generate and execute the command that will start the tmux session.

  def run!
    window_commands = @windows.map do |name, root_node|
      "tmux new-window -n '#{name}'\n" + root_node.tmux_split_commands +
        root_node.send_key_commands
    end
    temp_file = `mktemp temp_tmux_config.XXXX`.strip
    tmux_commands = window_commands.join("\n") + "\nrm -rf #{temp_file}"
    open(temp_file, 'w') {|f| f.write(tmux_commands)}
    exec "tmux new-session 'bash #{temp_file}'"
  end

end

end
