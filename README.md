# Decmux

Not really a gem but more of a combinator style library for writing tmux layout configuration intermixed with commands to be run in each tmux pane once the layout is configured. Once you have your configuration starting a tmux session with the config is as simple as `ruby config.rb`. Take a look at `examples/example.rb` to get you started.

## Installation

Add this line to your application's Gemfile:

    gem 'decmux', :git => 'git://github.com/davidk01/declarative-tmux-layouts.git', :branch => 'master'

And then execute:

    $ bundle

Or install it yourself as:

    $ git clone https://github.com/davidk01/declarative-tmux-layouts
    $ cd declarative-tmux-layouts && rake install

## Usage

Take a look at `examples/example.rb` for a very simple example of a layout configuration. To execute it and see
the results just run `ruby examples/example.rb`.

## Contributing

1. Fork it
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create new Pull Request
