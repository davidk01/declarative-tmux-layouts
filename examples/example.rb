require 'rubygems'
require 'decmux'

Decmux::Layout::configure! do

  window("ruby stuff") do
    cmd "echo top level first command."
    split(:horizontal) do 
      left.split(:vertical) do 
        cmd "echo left split command."
        top.cmd "echo left top command"
        bottom.cmd "echo left bottom command"
      end
      right.split(:vertical) do 
        cmd "echo right split command"
        top.cmd "echo right top command"
        bottom.cmd "echo right bottom command"
      end
    end
    cmd "echo last command for all nodes."
  end

  window("python stuff") do
    cmd "echo top level first command"
    split(:vertical) do 
      top.split(:horizontal) do 
        cmd "echo top split command."
        right.cmd "echo right top command"
      end
      bottom.split(:horizontal) do 
        cmd "echo bottom split command"
        left.cmd "echo left bottom command"
        right.cmd "echo right bottom command"
      end
    end
    cmd "echo last command for all nodes."
  end

  window('3 panes') do
    split(:horizontal) do
      right.split(:vertical)
    end
  end

end
