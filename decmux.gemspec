# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'decmux/version'

Gem::Specification.new do |spec|
  spec.name          = "decmux"
  spec.version       = Decmux::VERSION
  spec.authors       = ["david karapetyan"]
  spec.email         = ["dkarapetyan@scriptcrafty.com"]
  spec.description   = %q{Declarative tmux layouts.}
  spec.summary       = %q{Create the configuration and run it.}
  spec.homepage      = ""
  spec.license       = "BSD"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
